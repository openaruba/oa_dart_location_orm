import 'package:flutter_test/flutter_test.dart';
import 'package:oa_dart_location_orm/Endpoints/Endpoint.dart';

void main() {
  test('EndPoint.addParameterIfValueThere value passed', () {
    final url = EndPoint.addParameterIfValueThere("search", "search");
    expect(url, "&search=search");
  });

  test('EndPoint.addParameterIfValueThere nil passed', () {
    final url = EndPoint.addParameterIfValueThere("search", null);
    expect(url, "");
  });
}
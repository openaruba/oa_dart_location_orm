import 'package:flutter_test/flutter_test.dart';
import 'package:oa_dart_location_orm/Endpoints/RegionEndpoint.dart';

void main() {
  test('Test RegionEndpoint', () {
    final url = RegionEndpoint(1).uri.toString();
    expect(url, "regions?page=1");
  });
}
import 'package:flutter_test/flutter_test.dart';
import 'package:oa_dart_location_orm/Endpoints/StreetEndpoint.dart';

void main() {
  test('Test StreetEndpoint', () {
    final url = StreetEndpoint(1, "search", "2", "3", "4").uri.toString();
    expect(url, "streets?page=1&search=search&id=2&zoneId=3&regionId=4");
  });
}
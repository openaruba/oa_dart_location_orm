import 'package:flutter_test/flutter_test.dart';
import 'package:oa_dart_location_orm/Endpoints/AddressesEndpoint.dart';

void main() {
  test('Test AddressesEndpoint', () {
    final url = AddressEndpoint(1, "search", "1", "2", "3", "4").uri.toString();
    expect(url, "addresses?page=1&search=search&id=1&streetId=2&zoneId=3&regionId=4");
  });
}
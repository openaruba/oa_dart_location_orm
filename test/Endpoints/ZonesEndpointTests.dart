import 'package:flutter_test/flutter_test.dart';
import 'package:oa_dart_location_orm/Endpoints/ZonesEndpoint.dart';

void main() {
  test('Test StreetEndpoint', () {
    final url = ZonesEndpoint(1, "search", "2", "3").uri.toString();
    expect(url, "zones?page=1&search=search&id=2&regionId=3");
  });
}
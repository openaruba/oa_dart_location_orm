import 'dart:io';

class UserException implements Exception {

  final String userMessage;
  final int userCode;

  UserException(Exception exception) :
        this.userMessage = UserException._exceptionToUserMessage(exception),
        this.userCode = UserException._exceptionToUserCode(exception);

  static String _exceptionToUserMessage(Exception exception) {

    if (exception is SocketException) {
        return "There has been a connection error. Please check your connection and try again.";
    }
    return "Unknown error";
  }

  static int _exceptionToUserCode(Exception exception) {
    if (exception is SocketException) {
      return 1;
    }
    return 0;
  }
}

class Pages<Kind> {
  final int? page;
  final int? of;
  final List<Kind> objects;

  Pages(Map<String, dynamic> json, List<Kind> objects)
      : this.page = json["page"]
      , this.of = json["of"]
      , this.objects = objects;

  Pages.raw(int page, int of, List<Kind> objects):
        this.page = page,
        this.of = of,
        this.objects = objects;
}
library oa_dart_location_orm;

import 'package:http/http.dart' as http;
import 'package:oa_dart_location_orm/Endpoints/RegionEndpoint.dart';
import 'package:oa_dart_location_orm/Models/Pages.dart';
import 'dart:convert';
import 'package:oa_dart_location_orm/Environment.dart';

class Region {
  final String? id;
  final String? name;

  Region(Map<String, dynamic> json):
        id = json["id"],
        name = json["name"];

  Region.raw(String id, String name)
      : id = id,
        name = name;

  // Retrieve Regions
  static Future<Pages<Region>> all({int page = 1}) async {

    if(Environment.apiLocation == "") {
      throw "Please set your \"Environment.apiLocation\" to your servers location.";
    }

    // Create endpoint
    final endpoint = new RegionEndpoint(page);

    // Make request
    var response = await http.get(endpoint.uri);

    // decode json
    Map<String, dynamic> json = jsonDecode(response.body);

    // Create region objects
    final Iterable regionsJson = json["regions"];

    // Embed region in pages
    final List<Region> regions = List<Region>.from(regionsJson.map((model)=> Region(model)));

    // Return the pages
    return Pages<Region>(json, regions);
  }
}


import 'package:oa_dart_location_orm/Models/Street.dart';
import 'package:oa_dart_location_orm/Models/Pages.dart';
import 'package:oa_dart_location_orm/Models/Location.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:oa_dart_location_orm/Endpoints/AddressesEndpoint.dart';
import 'package:oa_dart_location_orm/Environment.dart';

class Address {
  final String? id;
  final String? number;

  final Street street;
  final Location location;

  String get readable {
    return (street.name ?? "") + " " + (number ?? "");
  }

  Address(Map<String, dynamic> json) :
        id = json["id"], number = json["number"],
        street = Street(json["street"]),
        location = Location(json["location"]);

  Address.raw(String id, String number, Street street, Location location) :
        id = id,
        number = number,
        street = street,
        location = location;

  // Retrieve Address
  static Future<Pages<Address>> all([int page = 1, String? search, String? id, String? regionId]) async {

    if(Environment.apiLocation == "") {
      throw "Please set your \"Environment.apiLocation\" to your servers location.";
    }

    // Create endpoint
    final endpoint = new AddressEndpoint(page, search, id, regionId);

    // Make request
    try {
      var response = await http.get(endpoint.uri);
      // decode json
      Map<String, dynamic> json = jsonDecode(response.body);

      // Create Address objects
      final Iterable addressesJson = json["addresses"];

      // Embed Address in pages
      final List<Address> addresses = List<Address>.from(
          addressesJson.map((model) => Address(model)));

      // Return the pages
      return Pages<Address>(json, addresses);
    }
    catch (e) {
      throw(e);
    }
  }
}
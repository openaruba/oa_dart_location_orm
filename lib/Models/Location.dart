class Location {
  final String latitude;
  final String longitude;

  double get latitudeDouble {
    return double.parse(latitude);
  }

  double get longitudeDouble {
      return double.parse(longitude);
  }

  Location(Map<String, dynamic> json)
      : this.latitude = json["latitude"],
        this.longitude = json["longitude"];

  Location.raw(String latitude, String longitude):
        this.latitude = latitude,
        this.longitude = longitude;
}
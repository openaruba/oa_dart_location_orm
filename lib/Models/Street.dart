import 'package:oa_dart_location_orm/Models/Pages.dart';
import 'package:oa_dart_location_orm/Models/Zone.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:oa_dart_location_orm/Endpoints/StreetEndpoint.dart';
import 'package:oa_dart_location_orm/Environment.dart';

import '../ORMCommon/UserException.dart';

class Street {
  final String? id;
  final String? name;
  final String? abbreviation;
  final int? gacCode;
  final Zone zone;

  Street(Map<String, dynamic> json):
        id = json["id"],
        name = json["name"],
        abbreviation = json["abbreviation"],
        gacCode = json["gacCode"] as int,
        zone = Zone(json["zone"]);

  Street.raw(String id, String name, String abbreviation, int gacCode, Zone zone)
      : id = id,
        name = name,
        abbreviation = abbreviation,
        gacCode = gacCode,
        zone = zone;

  bool filters(String filter) {
    final filterlc = filter.toLowerCase();
    return name!.toLowerCase().contains(filterlc)
        || abbreviation!.toLowerCase().contains(filterlc)
        || gacCode.toString().toLowerCase().contains(filterlc)
        || zone.name!.toLowerCase().contains(filterlc)
        || zone.region.name!.toLowerCase().contains(filterlc);
  }
  // Retrieve Street
  static Future<Pages<Street>> all([int page = 1, String? search, String? id, String? regionId]) async {

    if(Environment.apiLocation == "") {
      throw "Please set your \"Environment.apiLocation\" to your servers location.";
    }

    // Create endpoint
    final endpoint = new StreetEndpoint(page, search, id, regionId);

    // Make request
    try {
      var response = await http.get(endpoint.uri);

      // decode json
      Map<String, dynamic> json = jsonDecode(response.body);

      // Create Street objects
      final Iterable streetsJson = json["streets"];

      // Embed streets in pages
      final List<Street> streets = List<Street>.from(
          streetsJson.map((model) => Street(model))
      );

      // Return the pages
      return Pages<Street>(json, streets);
    } catch(error) {
        throw UserException(error as Exception);
    }
  }
}
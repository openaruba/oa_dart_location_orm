import 'package:oa_dart_location_orm/Endpoints/ZonesEndpoint.dart';
import 'package:oa_dart_location_orm/Models/Pages.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:oa_dart_location_orm/Environment.dart';
import 'package:oa_dart_location_orm/Models/Region.dart';

class Zone {
  final String? id;
  final String? name;
  final Region region;
  
  Zone(Map<String, dynamic> json)
      : id = json["id"]
      , name = json["name"]
      , region = Region(json["region"]);

  Zone.raw(String id, String name, Region region)
      : id = id,
        name = name,
        region = region;

  // Retrieve Zones
  static Future<Pages<Zone>> all([int page = 1, String? search, String? id, String? regionId]) async {
    if(Environment.apiLocation == "") {
        throw "Please set your \"Environment.apiLocation\" to your servers location.";
    }

    // Create endpoint
    final endpoint = new ZonesEndpoint(page, search, id, regionId);

    // Make request
    var response = await http.get(endpoint.uri);

    // decode json
    Map<String, dynamic> json = jsonDecode(response.body);

    // Create zone objects
    final Iterable regionsJson = json["zones"];

    // Embed zones in pages
    final List<Zone> zones = List<Zone>.from(regionsJson.map((model)=> Zone(model)));

    // Return the pages
    return Pages<Zone>(json, zones);
  }
}
class EndPoint{
  static String addParameterIfValueThere(String name, [String? value]) {
    return value == null
        ? ""
        : "&"+ name + "=" + value;
  }
}
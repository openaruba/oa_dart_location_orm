import 'package:oa_dart_location_orm/Environment.dart';
import 'package:oa_dart_location_orm/Endpoints/Endpoint.dart';

class StreetEndpoint {
  final Uri uri;
  StreetEndpoint([int? page, String? search, String? id, String? zoneId, String? regionId])
      : this.uri = Uri.parse(
      Environment.apiLocation
          + "streets?page=" + page.toString()
          + EndPoint.addParameterIfValueThere("search", search)
          + EndPoint.addParameterIfValueThere("id", id)
          + EndPoint.addParameterIfValueThere("zoneId", zoneId)
          + EndPoint.addParameterIfValueThere("regionId", regionId)
  );
}
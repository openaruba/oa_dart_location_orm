import 'package:oa_dart_location_orm/Environment.dart';

class RegionEndpoint {
  final Uri uri;
  RegionEndpoint(int page)
    : this.uri = Uri.parse(Environment.apiLocation + "regions?page=" + page.toString());
}
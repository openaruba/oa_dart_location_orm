import 'package:oa_dart_location_orm/Environment.dart';
import 'package:oa_dart_location_orm/Endpoints/Endpoint.dart';

class AddressEndpoint {
  final Uri uri;
  AddressEndpoint([int? page, String? search, String? id, String? streetId, String? zoneId, String? regionId])
      : this.uri = Uri.parse(
      Environment.apiLocation
          + "addresses?page=" + page.toString()
          + EndPoint.addParameterIfValueThere("search", search)
          + EndPoint.addParameterIfValueThere("id", id)
          + EndPoint.addParameterIfValueThere("streetId", streetId)
          + EndPoint.addParameterIfValueThere("zoneId", zoneId)
          + EndPoint.addParameterIfValueThere("regionId", regionId)
  );
}
import 'package:oa_dart_location_orm/Endpoints/Endpoint.dart';
import 'package:oa_dart_location_orm/Environment.dart';

class ZonesEndpoint {
  final Uri uri;
  ZonesEndpoint(int page, [String? search, String? id, String? regionId])
      : this.uri = Uri.parse(
        Environment.apiLocation
          + "zones?page=" + page.toString()
          + EndPoint.addParameterIfValueThere("search", search)
          + EndPoint.addParameterIfValueThere("id", id)
          + EndPoint.addParameterIfValueThere("regionId", regionId)
      );
}